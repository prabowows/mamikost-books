package com.example.agrprabo6477.ajobthing

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
//import com.google.android.gms.ads.MobileAds
import id.ai.mobilerecruitment.utils.Helpers


/**
 * Created by Prabowo Wahyu Sudarno.
 */
class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
        //MobileAds.initialize(this, "ca-app-pub-3940256099942544/6300978111");
    }

    override fun onCreate() {
        super.onCreate()
        Helpers.setRealmConfiguration(this)
        //Fabric.with(this, Crashlytics())
    }
}