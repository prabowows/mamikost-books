package com.example.agrprabo6477.ajobthing.models.response

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class Salary : RealmObject() {
    @SerializedName("currency")
    var currency: String? = ""
    @SerializedName("maximum")
    var maximum: String? = ""
    @SerializedName("minimum")
    var minimum: String? = ""
}