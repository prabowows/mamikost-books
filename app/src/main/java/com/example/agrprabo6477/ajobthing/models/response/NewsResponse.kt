package com.example.agrprabo6477.ajobthing.models.response


import com.google.gson.annotations.SerializedName

data class NewsResponse(
    @SerializedName("copyright")
    var copyright: String?,
    @SerializedName("response")
    var response: Response?,
    @SerializedName("status")
    var status: String?
)