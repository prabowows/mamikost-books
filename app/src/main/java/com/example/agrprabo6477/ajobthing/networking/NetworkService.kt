package id.astra.daihatsu.mobileservice.networking

import com.example.agrprabo6477.ajobthing.models.response.AvailableJobResponse
import com.example.agrprabo6477.ajobthing.models.response.JobResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface NetworkService {
    @GET("recommendation")
    fun GetAvailableJob(): Observable<JobResponse>


}
