package com.example.agrprabo6477.ajobthing.utils;

import android.content.Context;
import com.example.agrprabo6477.ajobthing.R;
import com.example.agrprabo6477.ajobthing.models.ListInt;
import com.example.agrprabo6477.ajobthing.models.ListString;
import com.example.agrprabo6477.ajobthing.models.response.JobResponse;
import com.example.agrprabo6477.ajobthing.models.response.Salary;
import com.example.agrprabo6477.ajobthing.utils.jsonParser.ArrayOrSingleDeserializer;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import id.astra.hso.stowadig.utils.jsonParser.IntegerRealmListConverter;
import id.astra.hso.stowadig.utils.jsonParser.StringRealmListConverter;
import io.realm.RealmList;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Prabowo Wahyu Sudarno.
 */

public class HelpersJava {


    public static String doHash256Hex(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        try {
            md.update(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bytesToHex(md.digest());
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    public static Gson getCustomGson(Context context) {
        return new GsonBuilder()
                .setDateFormat(context.getString(R.string.default_date_time_format))
                .registerTypeAdapter(new TypeToken<RealmList<ListInt>>() {
                        }.getType(),
                        new IntegerRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<ListString>>() {
                        }.getType(),
                        new StringRealmListConverter())
                .create();
    }

//    public static class PostArrayOrSingleDeserializer implements JsonDeserializer<Salary[]> {
//
//        private final Gson gson = new Gson();
//
//        @Override
//        public Salary[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//
//        }
//    }

    public static Gson getCustomGson2(Context context) {
        return new GsonBuilder()
                .setLenient()
                .setDateFormat(context.getString(R.string.default_date_time_format))
//                .registerTypeAdapter(Salary[].class, new ArrayOrSingleDeserializer())
                .registerTypeAdapter(new TypeToken<Salary>() {
                        }.getType(),
                        new ArrayOrSingleDeserializer())
                .registerTypeAdapter(new TypeToken<RealmList<ListInt>>() {
                        }.getType(),
                        new IntegerRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<ListString>>() {
                        }.getType(),
                        new StringRealmListConverter())
                .create();
                //.fromJson(JobResponse.toString(), Salary[].class);
    }


}
