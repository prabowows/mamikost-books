package com.example.agrprabo6477.ajobthing.utils.jsonParser

import com.example.agrprabo6477.ajobthing.models.ListInt
import com.example.agrprabo6477.ajobthing.models.response.JobDataFields
import com.example.agrprabo6477.ajobthing.models.response.Salary
import com.google.gson.*
import io.realm.RealmList
import java.lang.reflect.Type
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.google.gson.Gson
import com.google.gson.JsonDeserializer


class ArrayOrSingleDeserializer : JsonSerializer<Array<Salary>>, JsonDeserializer<Salary> {
    override fun serialize(src: Array<Salary>?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {

        val ja = JsonArray()
        if (src != null) {
            for (tag in src) {
                if (context != null) {
                    ja.add(context.serialize(tag))
                }
            }
        }
        return ja
    }




    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): Salary {
        var salary = Salary()

        var gson = Gson()
        if (json.isJsonArray) {
            val ja = json.asJsonArray
            for (je in ja) {
                val value = context.deserialize<Salary>(je, Salary::class.java)

                salary.maximum = value.maximum
                salary.minimum = value.minimum
                salary.currency = value.currency
            }

        }
        else if (json.isJsonObject){
            val ja = json.asJsonObject
            val value = gson.fromJson(json, Salary::class.java)
            //val value = context.deserialize<Salary>(ja, Salary::class.java)
            salary.maximum = value.maximum
            salary.minimum = value.minimum
            salary.currency = value.currency
        }
        return salary
    }

}