package com.example.agrprabo6477.ajobthing.utils

import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.models.response.JobDataFields
import io.realm.Realm

class JobHelper {

    fun save(job :JobData) {
        ModelHelper().update(job)
    }

    fun saveAndClear(listJob: List<JobData>) {
        deleteAll()
        ModelHelper().updateAll(listJob)
    }

    fun deleteAll() {
        ModelHelper().deleteAll(JobData::class.java)
    }

    fun getAll(realm: Realm): List<JobData> {
        var listResult: List<JobData> = arrayListOf()
        listResult = realm.copyFromRealm(ModelHelper().getAllSorted(realm, JobData::class.java,JobDataFields.DIFFERENT,true))
        return listResult
    }

    fun getJobById(realm : Realm, id : Int): JobData?{
        return ModelHelper().find(realm, JobData::class.java, JobDataFields.ID,id)
    }


}