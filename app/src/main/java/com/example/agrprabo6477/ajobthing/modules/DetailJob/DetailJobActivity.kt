package com.example.agrprabo6477.ajobthing.modules.DetailJob

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.agrprabo6477.ajobthing.R
import com.example.agrprabo6477.ajobthing.modules.Favorite.FavoritePresenter
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_job.*
import id.ai.mobilerecruitment.utils.Helpers
import kotlinx.android.synthetic.main.item_job.view.*
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast
import android.R.attr.name
import android.content.pm.ResolveInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.FragmentActivity
import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


class DetailJobActivity : AppCompatActivity() {

    private lateinit var presenter: DetailJobPresenter
    private lateinit var realm : Realm
    private var id = 0
    private lateinit var callbackManager : CallbackManager
    private lateinit var shareDialog: ShareDialog
    private var job : JobData = JobData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_job)
        realm = Realm.getDefaultInstance()
        presenter = DetailJobPresenter(this,realm)
        FacebookSdk.sdkInitialize(this.applicationContext)

        callbackManager = CallbackManager.Factory.create()
        shareDialog = ShareDialog(this)

        getBundle()
        setData()
        iv_detail_close.setOnClickListener { finish() }
        share_other.setOnClickListener { shareToOther() }
        share_wa.setOnClickListener { shareToWhatsapp() }
        share_link.setOnClickListener { copyLink() }
        share_twitter.setOnClickListener{shareTwitter(job.shareUrl!!)}
        share_fb.setOnClickListener { shareAppLinkViaFacebook("") }
    }

    private fun getBundle() {
        if (intent.hasExtra("id")) {
            id = intent.getIntExtra("id", 0)
        }
    }



    private fun shareTwitter(message: String) {
        val tweetIntent = Intent(Intent.ACTION_SEND)
        tweetIntent.putExtra(Intent.EXTRA_TEXT, "This is a Test.")
        tweetIntent.type = "text/plain"

        val packManager = packageManager
        val resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY)

        var resolved = false
        for (resolveInfo in resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.name
                )
                resolved = true
                break
            }
        }
        if (resolved) {
            startActivity(tweetIntent)
        } else {
            val i = Intent()
            i.putExtra(Intent.EXTRA_TEXT, message)
            i.action = Intent.ACTION_VIEW
            i.data = Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message))
            startActivity(i)
            Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show()
        }
    }

    private fun urlEncode(s: String): String {
        try {
            return URLEncoder.encode(s, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            //Log.wtf(FragmentActivity.TAG, "UTF-8 should always be supported", e)
            return ""
        }

    }


    private fun shareToWhatsapp() {
        val whatsappIntent = Intent(Intent.ACTION_SEND)
        whatsappIntent.type = "text/plain"
        whatsappIntent.setPackage("com.whatsapp")
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, job.shareUrl)
        try {
            startActivity(whatsappIntent)
        } catch (ex: android.content.ActivityNotFoundException) {
//            ToastHelper.MakeShortText("Whatsapp have not been installed.")
            Helpers.showToast(this,"Whatsapp have not been installed.",true)
        }

    }

    private fun shareToOther(){
        val intent2 = Intent()
        intent2.action = Intent.ACTION_SEND
        intent2.setType("text/plain")
        intent2.putExtra(Intent.EXTRA_TEXT, job.shareUrl)
        startActivity(Intent.createChooser(intent2, "Share via"))
    }

    private fun copyLink(){
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", job.shareUrl)
        clipboard!!.setPrimaryClip(clip)
        Helpers.showToast(this,"Link Copied",true)
    }

    private fun shareAppLinkViaFacebook(urlToShare: String) {
        var linkContent = ShareLinkContent.Builder()
            .setQuote(job.companyName)
            .setContentUrl(Uri.parse(job.shareUrl))
            .build()

        if(ShareDialog.canShow(ShareLinkContent::class.java)){
            shareDialog.show(linkContent)

        }


    }



    fun setData(){
        job = realm.copyFromRealm(presenter.getJob(id))
        Helpers.showClearImageFromURL(this,job.logo!!,iv_detail_photo)
        textView.text = "About ${job.companyName}"
        tv_detail_position.text = job.jobTitle
        tv_detail_company.text = "${job.companyName}"
        tv_detail_city.text = " - ${job.city}"
        wv_detail_responsibility.loadData(job.responsibility, "text/html", "UTF-8")
        wv_detail_requirement.loadData(job.requirement, "text/html", "UTF-8")
        tv_detail_about.text = job.description
        if(job.salary != null) {
            if (job.salary?.currency.equals("IDR", true)) {
                tv_detail_salary.text = "Rp.${Helpers.convertDoubleToDecimal(job.salary!!.minimum.toString())}-Rp.${Helpers.convertDoubleToDecimal(job.salary!!.maximum.toString())}"
            } else if (job.salary?.currency.equals("USD", true) ) {
                tv_detail_salary.text = "$${Helpers.convertDoubleToDecimal(job.salary!!.minimum.toString())}-$${Helpers.convertDoubleToDecimal(job.salary!!.maximum.toString())}"
            }
            else if (job.salary?.currency.equals("SGD", true) ) {
                tv_detail_salary.text = "S$${Helpers.convertDoubleToDecimal(job.salary!!.minimum.toString())}-S$${Helpers.convertDoubleToDecimal(job.salary!!.maximum.toString())}"
            }else if (job.salary?.currency.equals("EUR", true) ) {
                tv_detail_salary.text = "€${Helpers.convertDoubleToDecimal(job.salary!!.minimum.toString())}-€${Helpers.convertDoubleToDecimal(job.salary!!.maximum.toString())}"
            }
        }
        if (job.isFav!!) {
            ib_detail_love.setImageDrawable(getDrawable(R.drawable.ic_like_filled))
        } else {
            ib_detail_love.setImageDrawable(getDrawable(R.drawable.ic_like))
        }

        ib_detail_love.setOnClickListener {
            var thisJob = JobData()
            thisJob = job
            if(job.isFav!!) {
                thisJob.isFav = false
                JobHelper().save(thisJob)
                ib_detail_love.setImageDrawable(getDrawable(R.drawable.ic_like))
            }else{
                thisJob.isFav = true
                JobHelper().save(thisJob)
                ib_detail_love.setImageDrawable(getDrawable(R.drawable.ic_like_filled))
            }
        }

    }
}
