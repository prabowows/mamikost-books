package com.example.agrprabo6477.ajobthing.models

import io.realm.RealmObject
import io.realm.annotations.RealmClass

/**
 * Created by hendry on 7/21/17.
 */

@RealmClass
open class ListInt : RealmObject() {

    var value: Int? = null

}