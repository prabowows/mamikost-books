package com.example.agrprabo6477.ajobthing.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList

class JobResponse {

    @SerializedName("data")
    var jobData: List<JobData> = arrayListOf()
    @SerializedName("header")
    var header: String? = ""
    @SerializedName("status")
    var status: String? = ""
}