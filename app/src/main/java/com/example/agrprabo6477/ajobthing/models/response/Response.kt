package com.example.agrprabo6477.ajobthing.models.response


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("docs")
    var docs: List<Doc?>?,
    @SerializedName("meta")
    var meta: Meta?
)